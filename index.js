"use strict";

var numDivisions = 3;

var index = 0;

var points = [];
var normals = [];

var modelViewMatrix = [];
var projectionMatrix = [];
var rotationMatrix = [];
var translationMatrix = [];

var modelViewMatrixLoc, projectionMatrixLoc, rotationMatrixLoc, translationMatrixLoc;

var normalMatrix, normalMatrixLoc;

var isPaused = false;
var isMoving = true;
var moveCounter = 0;
var moveDistance = 0.0;
var distanceMultiplier = 0.25;

var axis = 0;
var xAxis = 0;
var yAxis = 1;
var zAxis = 2;
var dTheta = 5.0;

var flag = true;

var program;
var canvas, render, gl;

var left = -4.0;
var right = 4.0;
var ytop = 4.0;
var bottom = -4.0;

var near = -100.0;
var far = 100.0;
var radius = 1.0;
var theta = 0.0;
var phi = 0.0;
var dr = 5.0 * Math.PI / 180.0;

var at = vec3(0.0, 0.0, 0.0);
var up = vec3(0.0, 1.0, 0.0);


// 26 Camera Position
var eyes = [
    vec3(-5, 5, 5),
    vec3(0, 5, 5),
    vec3(5, 5, 5),
    vec3(-5, 0, 5),
    vec3(0, 0, 5),
    vec3(5, 0, 5),
    vec3(-5, -5, 5),
    vec3(0, -5, 5),
    vec3(5, -5, 5),

    vec3(-5, 5, 0),
    vec3(0, 5, 0.1),
    vec3(5, 5, 0),
    vec3(-5, 0, 0),
    vec3(5, 0, 0),
    vec3(-5, -5, 0),
    vec3(0, -5, 0.1),
    vec3(5, -5, 0),

    vec3(-5, 5, -5),
    vec3(0, 5, -5),
    vec3(5, 5, -5),
    vec3(-5, 0, -5),
    vec3(0, 0, -5),
    vec3(5, 0, -5),
    vec3(-5, -5, -5),
    vec3(0, -5, -5),
    vec3(5, -5, -5),
]

var eye = eyes[4];

var position = 4;

// Position state
var nextpos = [
    {
        left: 9,
        right: 1,
        down: 3
    },
    {
        up: 10,
        left: 0,
        right: 2,
        down: 4
    },
    {
        left: 1,
        right: 11,
        down: 5
    },
    {
        up: 0,
        left: 12,
        right: 4,
        down: 6
    },
    {
        up: 1,
        left: 3,
        right: 5,
        down: 7
    },
    {
        up: 2,
        left: 4,
        right: 13,
        down: 8
    },
    {
        up: 3,
        left: 14,
        right: 7
    },
    {
        up: 4,
        left: 6,
        right: 8,
        down: 15
    },
    {
        up: 5,
        left: 7,
        right: 16
    },
    {
        up: 10,
        left: 17,
        right: 0,
        down: 12
    },
    {
        up: 18,
        left: 9,
        right: 11,
        down: 1
    },
    {
        up: 10,
        left: 2,
        right: 19,
        down: 13
    },
    {
        up: 9,
        left: 20,
        right: 3,
        down: 14
    },
    {
        up: 11,
        left: 5,
        right: 22,
        down: 16
    },
    {
        up: 12,
        left: 23,
        right: 6,
        down: 15
    },
    {
        up: 7,
        left: 16,
        right: 14,
        down: 24
    },
    {
        up: 13,
        left: 8,
        right: 25,
        down: 15
    },
    {
        left: 18,
        right: 9,
        down: 20
    },
    {
        up: 10,
        left: 19,
        right: 17,
        down: 21
    },
    {
        left: 11,
        right: 18,
        down: 22
    },
    {
        up: 17,
        left: 21,
        right: 12,
        down: 23
    },
    {
        up: 18,
        left: 22,
        right: 21,
        down: 24
    },
    {
        up: 19,
        left: 13,
        right: 21,
        down: 25
    },
    {
        up: 20,
        left: 24,
        right: 19
    },
    {
        up: 21,
        left: 25,
        right: 23,
        down: 15
    },
    {
        up: 22,
        left: 16,
        right: 24
    }
]

var bezier = function(u) {
    var b =new Array(4);
    var a = 1-u;
    b[3] = a*a*a;
    b[2] = 3*a*a*u;
    b[1] = 3*a*u*u;
    b[0] = u*u*u;
    return b;
}

var nbezier = function(u) {
    var b = [];
    b.push(3*u*u);
    b.push(3*u*(2-3*u));
    b.push(3*(1-4*u+3*u*u));
    b.push(-3*(1-u)*(1-u));
    return b;
}

window.onload = function init()  {
    canvas = document.getElementById( "gl-canvas" );

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

    gl.viewport( 0, 0, canvas.width, canvas.height );

    gl.clearColor( 0.0, 0.0, 0.0, 0.1 );

    gl.enable(gl.DEPTH_TEST);


    var sum = [0, 0, 0];
    for(var i = 0; i<306; i++) for(j=0; j<3; j++) {
        sum[j] += vertices[i][j];
        for(j=0; j<3; j++) sum[j]/=306;
            for(var i = 0; i<306; i++) for(j=0; j<2; j++) {
                vertices[i][j] -= sum[j]/2
                    for(var i = 0; i<306; i++) for(j=0; j<3; j++) {
                        vertices[i][j] *= 2;
                    }
            }
    }


    var h = 1.0/numDivisions;

    var patch = new Array(numTeapotPatches);
    for(var i=0; i<numTeapotPatches; i++) patch[i] = new Array(16);
    for(var i=0; i<numTeapotPatches; i++)
        for(j=0; j<16; j++) {
            patch[i][j] = vec4([vertices[indices[i][j]][0],
             vertices[indices[i][j]][2],
                vertices[indices[i][j]][1], 1.0]);
    }


    for ( var n = 0; n < numTeapotPatches; n++ ) {


    var data = new Array(numDivisions+1);
    for(var j = 0; j<= numDivisions; j++) data[j] = new Array(numDivisions+1);
    for(var i=0; i<=numDivisions; i++) for(var j=0; j<= numDivisions; j++) {
        data[i][j] = vec4(0,0,0,1);
        var u = i*h;
        var v = j*h;
        var t = new Array(4);
        for(var ii=0; ii<4; ii++) t[ii]=new Array(4);
        for(var ii=0; ii<4; ii++) for(var jj=0; jj<4; jj++) {
            t[ii][jj] = bezier(u)[ii]*bezier(v)[jj];
        }


        for(var ii=0; ii<4; ii++) for(var jj=0; jj<4; jj++) {
            temp = vec4(patch[n][4*ii+jj]);
            temp = scale( t[ii][jj], temp);
            data[i][j] = add(data[i][j], temp);
        }
    }

    var ndata = new Array(numDivisions+1);
    for(var j = 0; j<= numDivisions; j++) ndata[j] = new Array(numDivisions+1);
    var tdata = new Array(numDivisions+1);
    for(var j = 0; j<= numDivisions; j++) tdata[j] = new Array(numDivisions+1);
    var sdata = new Array(numDivisions+1);
    for(var j = 0; j<= numDivisions; j++) sdata[j] = new Array(numDivisions+1);
    for(var i=0; i<=numDivisions; i++) for(var j=0; j<= numDivisions; j++) {
        ndata[i][j] = vec4(0,0,0,0);
        sdata[i][j] = vec4(0,0,0,0);
        tdata[i][j] = vec4(0,0,0,0);
        var u = i*h;
        var v = j*h;
        var tt = new Array(4);
        for(var ii=0; ii<4; ii++) tt[ii]=new Array(4);
        var ss = new Array(4);
        for(var ii=0; ii<4; ii++) ss[ii]=new Array(4);

        for(var ii=0; ii<4; ii++) for(var jj=0; jj<4; jj++) {
            tt[ii][jj] = nbezier(u)[ii]*bezier(v)[jj];
            ss[ii][jj] = bezier(u)[ii]*nbezier(v)[jj];
        }

        for(var ii=0; ii<4; ii++) for(var jj=0; jj<4; jj++) {
            var temp = vec4(patch[n][4*ii+jj]); ;
            temp = scale( tt[ii][jj], temp);
            tdata[i][j] = add(tdata[i][j], temp);

            var stemp = vec4(patch[n][4*ii+jj]); ;
            stemp = scale( ss[ii][jj], stemp);
            sdata[i][j] = add(sdata[i][j], stemp);

        }
        temp = cross(tdata[i][j], sdata[i][j])

        ndata[i][j] =  normalize(vec4(temp[0], temp[1], temp[2], 0));
    }

    for(var i=0; i<numDivisions; i++) for(var j =0; j<numDivisions; j++) {
        points.push(data[i][j]);
        normals.push(ndata[i][j]);

        points.push(data[i+1][j]);
        normals.push(ndata[i+1][j]);

        points.push(data[i+1][j+1]);
        normals.push(ndata[i+1][j+1]);

        points.push(data[i][j]);
        normals.push(ndata[i][j]);

        points.push(data[i+1][j+1]);
        normals.push(ndata[i+1][j+1]);

        points.push(data[i][j+1]);
        normals.push(ndata[i][j+1]);
        index+= 6;
        }
    }

    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer);
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW );


    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );


    var nBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, nBuffer);
    gl.bufferData( gl.ARRAY_BUFFER, flatten(normals), gl.STATIC_DRAW );

    var vNormal = gl.getAttribLocation( program, "vNormal" );
    gl.vertexAttribPointer( vNormal, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vNormal);

    modelViewMatrixLoc = gl.getUniformLocation( program, "modelViewMatrix" );
    projectionMatrixLoc = gl.getUniformLocation( program, "projectionMatrix" );
    rotationMatrixLoc = gl.getUniformLocation(program, "rotationMatrix");
    translationMatrixLoc = gl.getUniformLocation(program, "translationMatrix");

    normalMatrixLoc = gl.getUniformLocation( program, "normalMatrix" );

    var lightPosition = vec4(0.0, 0.0, 20.0, 0.0 );
    var lightAmbient = vec4(0.2, 0.2, 0.2, 1.0 );
    var lightDiffuse = vec4( 1.0, 1.0, 1.0, 1.0 );
    var lightSpecular = vec4( 1.0, 1.0, 1.0, 1.0 );

    var materialAmbient = vec4( 1.0, 0.0, 1.0, 1.0 );
    var materialDiffuse = vec4( 1.0, 0.8, 1.0, 1.0 );
    var materialSpecular = vec4( 1.0, 0.8, 1.0, 1.0 );
    var materialShininess = 10.0;

    var ambientProduct = mult(lightAmbient, materialAmbient);
    var diffuseProduct = mult(lightDiffuse, materialDiffuse);
    var specularProduct = mult(lightSpecular, materialSpecular);

    gl.uniform4fv( gl.getUniformLocation(program, "ambientProduct"),flatten(ambientProduct ));
    gl.uniform4fv( gl.getUniformLocation(program, "diffuseProduct"), flatten(diffuseProduct) );
    gl.uniform4fv( gl.getUniformLocation(program, "specularProduct"),flatten(specularProduct));
    gl.uniform4fv( gl.getUniformLocation(program, "lightPosition"), flatten(lightPosition ));
    gl.uniform1f( gl.getUniformLocation(program, "shininess"),materialShininess );


    var keyDownHandler = function (event) {
        switch (event.keyCode) {
            case 0x25:
                // Arrow Left Key
                var next = nextpos[position].left;
                if (next !== undefined) {
                    position = next;
                    smoothRotate2Absolute(eyes[position]);
                }
                break;
            case 0x26:
                // Arrow Up Key
                var next = nextpos[position].up;
                if (next !== undefined) {
                    position = next;
                    smoothRotate2Absolute(eyes[position]);
                }
                break;
            case 0x27:
                // Arrow Right Key
                var next = nextpos[position].right;
                if (next !== undefined) {
                    position = next;
                    smoothRotate2Absolute(eyes[position]);
                }
                break;
            case 0x28:
                // Arrow Down Key
                var next = nextpos[position].down;
                if (next !== undefined) {
                    position = next;
                    smoothRotate2Absolute(eyes[position]);
                }
                break;
        }
    }

    document.addEventListener('keydown', keyDownHandler, false);

    render();
}

render = function(){
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    modelViewMatrix = lookAt(eye, at, up);
    modelViewMatrix[3] = [0, 0, 0, 2]

    gl.uniformMatrix4fv(modelViewMatrixLoc, false, flatten(modelViewMatrix));

    projectionMatrix = ortho(left, right, bottom, ytop, near, far);
    gl.uniformMatrix4fv(projectionMatrixLoc, false, flatten(projectionMatrix));

    normalMatrix = [
        vec3(modelViewMatrix[0][0], modelViewMatrix[0][1], modelViewMatrix[0][2]),
        vec3(modelViewMatrix[1][0], modelViewMatrix[1][1], modelViewMatrix[1][2]),
        vec3(modelViewMatrix[2][0], modelViewMatrix[2][1], modelViewMatrix[2][2])
    ];

    gl.uniformMatrix3fv(normalMatrixLoc, false, flatten(normalMatrix) );

    gl.drawArrays( gl.TRIANGLES, 0, index);
    for(var i=0; i<index; i+=3) gl.drawArrays( gl.LINE_LOOP, i, 3 );

    var move_x = 0.0;
    var move_y = 0.0;
    var move_z = 0.0;

    // Moving teapot
    if (isMoving) {
        if(moveCounter < 25) {
            moveCounter += 1
            moveDistance += distanceMultiplier;
            move_z = moveDistance;
        }
        else if (moveCounter < 50) {
            moveCounter += 1
            moveDistance -= distanceMultiplier;
            move_z = moveDistance;
        }
        else if(moveCounter < 75) {
            moveCounter += 1
            moveDistance -= distanceMultiplier;
            move_z = moveDistance;
        }
        else if (moveCounter < 100) {
            moveCounter += 1
            moveDistance += distanceMultiplier;
            move_z = moveDistance;
        }
        else if(moveCounter < 125) {
            moveCounter += 1
            moveDistance += distanceMultiplier;
            move_x = moveDistance;
        }
        else if (moveCounter < 150) {
            moveCounter += 1
            moveDistance -= distanceMultiplier;
            move_x = moveDistance;
        }
        else if(moveCounter < 175) {
            moveCounter += 1
            moveDistance -= distanceMultiplier;
            move_x = moveDistance;
        }
        else if (moveCounter < 200) {
            moveCounter += 1
            moveDistance += distanceMultiplier;
            move_x = moveDistance;
        }
        else if(moveCounter < 225) {
            moveCounter += 1
            moveDistance -= distanceMultiplier;
            move_y = moveDistance;
        }
        else if (moveCounter < 250) {
            moveCounter += 1
            moveDistance += distanceMultiplier;
            move_y = moveDistance;
        }
        else if(moveCounter < 275) {
            moveCounter += 1
            moveDistance += distanceMultiplier;
            move_y = moveDistance;
        }
        else if (moveCounter < 300) {
            moveCounter += 1
            moveDistance -= distanceMultiplier;
            move_y = moveDistance;
        }
        else {
            moveCounter = 0;
            moveDistance = 0.0;
        }

        // Disable rotation
        rotationMatrix = rotateY(0.0);
        translationMatrix = translate(move_x, move_y, move_z);

        gl.uniformMatrix4fv(rotationMatrixLoc, false, flatten(rotationMatrix));
        gl.uniformMatrix4fv(translationMatrixLoc, false, flatten(translationMatrix));
    }

    requestAnimFrame(render);
}

// Mengimplementasikan tombol Pause, teapot berhenti bergerak
function pause() {
    isMoving = !isMoving;
    if (isPaused) {
        document.getElementById("pause").classList.remove("active");
        isPaused = false;
    } else {
        document.getElementById("pause").classList.add("active");
        isPaused = true;
    }
}

// Rotate camera smoothly using near, far, radius, theta, and phi
async function smoothRotate(_near, _far, _radius, _theta, _phi) {
    var period = 1000;
    var fps = 30;
    var step = 1000/fps;

    var nearStep = _near/fps;
    var farStep = _far/fps;
    var radiusStep = _radius/fps;
    var thetaStep = _theta/fps;
    var phiStep = _phi/fps;

    let update = () => {
        near += nearStep;
        far += farStep;
        radius += radiusStep;
        theta += thetaStep;
        phi += phiStep;
    }

    for (let ms = 0; ms < period; ms += step) {
        await createPromise(update, step);
    }
}

// Rotate camera smoothly only using eye vector
async function smoothRotate2(_eye) {
    var period = 1000;
    var fps = 30;
    var step = period/fps;

    var eyeStep = scale(1/step, _eye);
    var target = add(eye, _eye);

    let update = () => {
        eye = add(eye, eyeStep);
    }

    for (let ms = 0; ms < period; ms += step) {
        await createPromise(update, step);
    }

    var error = subtract(target, eye);
    eye = add(eye, error);
}

// Rotate camera smoothly to absolute value using near, far, radius, theta, and phi
async function smoothRotateAbsolute(_near, _far, _radius, _theta, _phi) {
    var nearChange = ((near > _near) ? -1.0 : 1.0) * Math.abs(near - _near);
    var farChange = ((far > _far) ? -1.0 : 1.0) * Math.abs(far - _far);
    var radiusChange = ((radius > _radius) ? -1.0 : 1.0) * Math.abs(radius - _radius);
    var thetaChange = ((theta > _theta) ? -1.0 : 1.0) * Math.abs(theta - _theta);
    var phiChange = ((phi > _phi) ? -1.0 : 1.0) * Math.abs(phi - _phi);
    smoothRotate(nearChange, farChange, radiusChange, thetaChange, phiChange);
}

// Rotate camera smoothly to absolute value using eye vector
async function smoothRotate2Absolute(_eye) {
    var eyeChange = vec3(
        ((eye[0] > _eye[0]) ? -1.0 : 1.0) * Math.abs(eye[0] - _eye[0]),
        ((eye[1] > _eye[1]) ? -1.0 : 1.0) * Math.abs(eye[1] - _eye[1]),
        ((eye[2] > _eye[2]) ? -1.0 : 1.0) * Math.abs(eye[2] - _eye[2])
    );
    smoothRotate2(eyeChange);
}

var createPromise = (func, timeout) => {
    return new Promise(resolve => {
        func();
        setTimeout(() => {
            resolve("Success!")
        }, timeout);
    })
}