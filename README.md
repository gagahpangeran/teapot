# 418 Teapot WebGL

_Repository_ ini berisi kode sumber untuk program 418 Teapot yang dibuat untuk memenuhi pengerjaan **nomor dua** Ujian Tengah Semester mata kuliah Grafika Komputer Semester Genap 2019/2020, Fakultas Ilmu Komputer, Universitas Indonesia.

![418 Teapot](https://files.catbox.moe/36uo8h.png)
![418 Teapot Different Position](https://files.catbox.moe/3186m5.png)

## Pembagian Tugas Pengembang

Gagah Pangeran Rosfatiputra (1706039566)

- Memasukkan _vertex teapot_ dari Chap11.
- Membuat peletakkan 26 kamera.
- Membuat _mapping keyboard_ untuk berpindah kamera.
- Membuat _state_ perpindahan kamera.

Giovan Isa Musthofa (1706040126)

- Membuat _template handler keydown event_ dari Chap5.
- Membuat animasi transisi kamera yang halus.
- Membuat matrix translasi _teapot_ supaya di-_apply_ sebelum matrix _viewmodel_ atau _projection_, bukan setelahnya.
- Eksperimen matrix rotasi _teapot_, tapi tidak diimplementasi sepenuhnya karena masalah _lighting_ yang masih mengikuti rotasi teapot sehingga membingungkan.

Roshani Ayu Pranasti (1706026052)

- Membuat animasi _teapot_ bergerak maju, mundur, kiri, kanan, atas, dan bawah.
- Mengimplementasikan fungsi untuk tombol _Pause_.
- Membuat seluruh tampilan HTML dan CSS.
- Membuat readme.md dan pdf.

## Deskripsi Program

Sebuah objek berbentuk _teapot_ yang memiliki 26 posisi kamera pada kotak batas atas _world scene_. Kamera tersebut mengawasi atau mengarah ke _teapot_ yang terus bergerak hingga batas-batas kotak. Gerakan teapot dapat dihentikan melalui tombol _Pause_. Posisi _teapot_ secara _default_ berada di origin/pusat _world scene_ (koordinat (0,0,0)).

## Petunjuk Pemakaian

1. Membuka file **index.html** dan menjalankannya di **browser** laptop atau komputer.
2. Pengubahan posisi kamera dilakukan dengan menggunakan tombol _keyboard_ panah (**atas** - **bawah** - **kiri** - **kanan**) sesuai posisi yang telah ditentukan dan sesuai arahnya.